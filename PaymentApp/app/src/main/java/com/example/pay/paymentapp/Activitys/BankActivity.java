package com.example.pay.paymentapp.Activitys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.pay.paymentapp.Adapters.BankRecyclerAdapter;
import com.example.pay.paymentapp.Adapters.RecyclerItemAdapter;
import com.example.pay.paymentapp.DataModel.Bank;
import com.example.pay.paymentapp.DataModel.Payment;
import com.example.pay.paymentapp.R;
import com.example.pay.paymentapp.Servicios.ClienteRetrofit;
import com.example.pay.paymentapp.Servicios.PayApiServices;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BankActivity extends AppCompatActivity {
    private String idPay;
    BankRecyclerAdapter rcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank);
        idPay=getIntent().getExtras().getString("idPay");
        getBank();
    }
    public void getBank() {

        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(getString(R.string.BASE_URL));

        OkHttpClient client = ClienteRetrofit.getSSLConfig(this);
        Retrofit retrofit = builder.client(client).addConverterFactory(GsonConverterFactory.create()).build();
        retrofit.create(PayApiServices.class);
        getBankApi(retrofit);


    }

    public void getBankApi(Retrofit retrofit) {


        PayApiServices servicio = retrofit.create(PayApiServices.class);
        //Accede al servicio de cancelar turno donde el parametro ids= id del turno
        Call<ArrayList<Bank>> loginCall = servicio.getBanks("444a9ef5-8a6b-429f-abdf-587639155d88",idPay);
        //Ejecuta una llamada asicnrona
        loginCall.enqueue(new Callback<ArrayList<Bank>>() {
            @Override
            public void onResponse(Call<ArrayList<Bank>> call, Response<ArrayList<Bank>> response) {

                if (response.isSuccessful()) {
                    gridImg(response.body());
                } else {

                }
            }

            @Override
            public void onFailure(Call<ArrayList<Bank>> call, Throwable t) {
                System.out.println("Falla al obtener respuesta: " + t.getMessage());

            }
        });


    }
    public void gridImg(ArrayList<Bank> lotesRes) {
        GridLayoutManager lLayout = new GridLayoutManager(BankActivity.this, 1);
        RecyclerView rView = (RecyclerView) findViewById(R.id.recycler_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);
        rcAdapter = new BankRecyclerAdapter(BankActivity.this, lotesRes, BankActivity.this);
        rView.setAdapter(rcAdapter);
    }
}
