package com.example.pay.paymentapp.Model.APIs;

import android.content.Context;

import com.example.pay.paymentapp.R;
import com.example.pay.paymentapp.Servicios.ClienteRetrofit;
import com.example.pay.paymentapp.Servicios.PayApiServices;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Andrea on 21/07/2018.
 */

public class ClientAPI {

    private static PayApiServices instance;

    public static PayApiServices getClient(Context context) {
       // if(instance == null) {
            OkHttpClient localClient = ClienteRetrofit.getSSLConfig(context);

            assert localClient != null;
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(context.getResources().getString(R.string.BASE_URL))
                    .client(localClient)
                    .addConverterFactory(GsonConverterFactory.create());
            instance = builder.build()
                    .create(PayApiServices.class);
       // }
        return instance;
    }
}

