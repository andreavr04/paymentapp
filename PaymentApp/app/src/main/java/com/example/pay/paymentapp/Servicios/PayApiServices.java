package com.example.pay.paymentapp.Servicios;

import com.example.pay.paymentapp.DataModel.Bank;
import com.example.pay.paymentapp.DataModel.Mensaje;
import com.example.pay.paymentapp.DataModel.Payer;
import com.example.pay.paymentapp.DataModel.PayerResult;
import com.example.pay.paymentapp.DataModel.Payment;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Andrea on 20/07/2018.
 */

public interface PayApiServices {

    @GET("/v1/payment_methods")
    Call<ResponseBody> getPaymentMethods(@Query("public_key") String public_key);

    @GET("/v1/payment_methods")
    Call<ArrayList<Payment>> getPaymentMethodsP(@Query("public_key") String public_key);

    @GET("/v1/payment_methods/card_issuers")
    Call<ArrayList<Bank>> getBanks(@Query("public_key") String public_key, @Query("payment_method_id") String payment_method_id);

    @GET("/v1/payment_methods/installments")
    Call<ArrayList<PayerResult>> getPayers(@Query("public_key") String public_key, @Query("amount") String amount, @Query("payment_method_id") String payment_method_id, @Query("issuer.id") String issuerId);


}
