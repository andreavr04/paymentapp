package com.example.pay.paymentapp.DataModel;

import java.util.ArrayList;

/**
 * Created by Andrea on 28/07/2018.
 */

public class Payer {

    private ArrayList<Mensaje> payer_costs;

    public ArrayList<Mensaje> getPayer_costs() {
        return payer_costs;
    }

    public void setPayer_costs(ArrayList<Mensaje> payer_costs) {
        this.payer_costs = payer_costs;
    }
}
