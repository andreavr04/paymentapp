package com.example.pay.paymentapp.Activitys;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.pay.paymentapp.Adapters.RecyclerItemAdapter;
import com.example.pay.paymentapp.DataModel.Payment;
import com.example.pay.paymentapp.Presenter.PaymentPresenter;
import com.example.pay.paymentapp.R;
import com.example.pay.paymentapp.Servicios.ClienteRetrofit;
import com.example.pay.paymentapp.Servicios.PayApiServices;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PaymentActivity extends AppCompatActivity {

    PaymentPresenter paymentPresenter = new PaymentPresenter(PaymentActivity.this);
    RecyclerItemAdapter rcAdapter;
    private SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        prefs = getSharedPreferences("com.nuestraapp.app", Context.MODE_PRIVATE);
        cambiarContraseña();
        // getPaymentMethods();
    }

    public void cambiarContraseña() {

        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(getString(R.string.BASE_URL));

        OkHttpClient client = ClienteRetrofit.getSSLConfig(this);
        Retrofit retrofit = builder.client(client).addConverterFactory(GsonConverterFactory.create()).build();
        retrofit.create(PayApiServices.class);
        cambiarContraseñaApi(retrofit);


    }

    public void cambiarContraseñaApi(Retrofit retrofit) {


        PayApiServices servicio = retrofit.create(PayApiServices.class);
        //Accede al servicio de cancelar turno donde el parametro ids= id del turno
        Call<ArrayList<Payment>> loginCall = servicio.getPaymentMethodsP("444a9ef5-8a6b-429f-abdf-587639155d88");
        //Ejecuta una llamada asicnrona
        loginCall.enqueue(new Callback<ArrayList<Payment>>() {
            @Override
            public void onResponse(Call<ArrayList<Payment>> call, Response<ArrayList<Payment>> response) {

                if (response.isSuccessful()) {
                    gridImg(response.body());
                } else {

                }
            }

            @Override
            public void onFailure(Call<ArrayList<Payment>> call, Throwable t) {
                System.out.println("Falla al obtener respuesta: " + t.getMessage());

            }
        });


    }



    public void getPaymentMethods() {
        paymentPresenter.getPayment(new Runnable() {
            @Override
            public void run() {
                System.out.println();
            }
        });

    }


    public void gridImg(ArrayList<Payment> lotesRes) {
        GridLayoutManager lLayout = new GridLayoutManager(PaymentActivity.this, 1);
        RecyclerView rView = (RecyclerView) findViewById(R.id.recycler_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);
        rcAdapter = new RecyclerItemAdapter(PaymentActivity.this, lotesRes, PaymentActivity.this);
        rView.setAdapter(rcAdapter);
    }

}
