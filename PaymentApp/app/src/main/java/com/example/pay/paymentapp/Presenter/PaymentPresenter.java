package com.example.pay.paymentapp.Presenter;

import android.content.Context;

import com.example.pay.paymentapp.Model.PaymentModel;

/**
 * Created by Andrea on 21/07/2018.
 */

public class PaymentPresenter {

    private Context context;
    PaymentModel paymentModel;

    public PaymentPresenter(Context context) {
        this.context = context;
        this.paymentModel = new PaymentModel(context);
    }

    public void getPayment(Runnable onResponse) {
        paymentModel.getMethodsPayment(onResponse);
    }

    /*public ArrayList<TurnosRespuesta> returnTurnos() {
        return turnosModel.returnTurnos();
    }*/


}
