package com.example.pay.paymentapp.Model;

import android.content.Context;
import android.widget.Toast;

import com.example.pay.paymentapp.DataModel.Payment;
import com.example.pay.paymentapp.Model.APIs.ClientAPI;
import com.example.pay.paymentapp.R;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Andrea on 21/07/2018.
 */

public class PaymentModel {

    Context context;


    public PaymentModel(Context context) {
        this.context = context;
    }



    public void getMethodsPayment(final Runnable onResponse) {
        ClientAPI.getClient(context).getPaymentMethods(context.getResources().getString(R.string.public_key)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ResponseBody resultados = response.body();
                if(resultados!=null) {
                    //turnosRes = resultados.getVistaModTurnos().get$values();
                    if (onResponse != null) onResponse.run();
                } else {
                    //turnosRes.clear();
                    if (onResponse != null) onResponse.run();
                    System.out.println("Error en la respuesta turnos");
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(context, "Ocurrió un error", Toast.LENGTH_SHORT).show();
                System.out.println("Error en la respuesta turnos fail: " + t.getMessage() + "call: " + call.toString());
            }
        });
    }

}
