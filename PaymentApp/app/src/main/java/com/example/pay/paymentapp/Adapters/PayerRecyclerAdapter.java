package com.example.pay.paymentapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pay.paymentapp.Activitys.MontoActivity;
import com.example.pay.paymentapp.DataModel.Bank;
import com.example.pay.paymentapp.DataModel.Mensaje;
import com.example.pay.paymentapp.DataModel.Payer;
import com.example.pay.paymentapp.R;
import com.example.pay.paymentapp.Servicios.PicassoTrustAll;
import com.squareup.picasso.Transformation;

import java.util.List;

/**
 * Created by Andrea on 28/07/2018.
 */

public class PayerRecyclerAdapter extends RecyclerView.Adapter<PayerRecyclerHolders> {

    private List<Mensaje> itemList;
    private Context context;
    private SharedPreferences prefs;
    private String autorization;
    private String ids = "";
    public static final String MESSAGE_PROGRESS = "message_progress";
    private static final int PERMISSION_REQUEST_CODE = 1;
    Activity activity;


    public PayerRecyclerAdapter(Context context, List<Mensaje> itemList, Activity activity) {
        this.itemList = itemList;
        this.context = context;
        this.activity = activity;
        prefs = context.getSharedPreferences("com.nuestraapp.app", Context.MODE_PRIVATE);


    }

    @Override
    public PayerRecyclerHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout_pay, null);
        PayerRecyclerHolders rcv = new PayerRecyclerHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final PayerRecyclerHolders holder, final int position) {
        if (itemList.get(position).getRecommended_message() != null) {
            holder.title.setText(itemList.get(position).getRecommended_message() + "");
        } else {
            holder.title.setText("Sin información");
        }

        Transformation transformation = new Transformation() {

            @Override
            public Bitmap transform(Bitmap source) {
                int targetWidth = 500;

                double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                int targetHeight = (int) (targetWidth * aspectRatio);
                Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                if (result != source) {
                    // Same bitmap is returned if sizes are the same
                    source.recycle();
                }
                return result;
            }

            @Override
            public String key() {
                return "transformation" + " desiredWidth";
            }
        };


//Next Screen
        holder.viewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(context, MontoActivity.class);
                prefs.edit().putString("com.nuestraapp.app.mensaje",itemList.get(position).getRecommended_message()).commit();
                i.putExtra("mensaje",itemList.get(position).getRecommended_message());
                i.putExtra("showData",true);
                context.startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}