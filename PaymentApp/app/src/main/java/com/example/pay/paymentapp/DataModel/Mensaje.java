package com.example.pay.paymentapp.DataModel;

/**
 * Created by Andrea on 30/07/2018.
 */

public class Mensaje {
    private String recommended_message;
    private Double installment_amount;

    public String getRecommended_message() {
        return recommended_message;
    }

    public void setRecommended_message(String recommended_message) {
        this.recommended_message = recommended_message;
    }

    public Double getInstallment_amount() {
        return installment_amount;
    }

    public void setInstallment_amount(Double installment_amount) {
        this.installment_amount = installment_amount;
    }
}
