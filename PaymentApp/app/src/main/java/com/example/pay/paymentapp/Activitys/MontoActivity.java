package com.example.pay.paymentapp.Activitys;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.pay.paymentapp.R;

public class MontoActivity extends AppCompatActivity {

    public static String public_key;
    private Button nextBtn;
    private EditText mountEdit;
    private SharedPreferences prefs;
    private String mount;
    private String paymentName;
    private String bankName;
    private String mensaje;
    private TextView mountTxt;
    private TextView paymentNameTxt;
    private TextView bankNameTxt;
    private TextView mensajeTxt;

    AlertDialog.Builder dialogo;
    AlertDialog alertDialogo;
    boolean band = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monto);
        prefs = getSharedPreferences("com.nuestraapp.app", Context.MODE_PRIVATE);
        public_key = "444a9ef5-8a6b-429f-abdf-587639155d88";
        nextBtn = (Button) findViewById(R.id.nextBtn);
        mountEdit = (EditText) findViewById(R.id.mountEdit);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //save Mount
                prefs.edit().putString("com.nuestraapp.app.mount", mountEdit.getText().toString()).commit();
                startActivity(new Intent(MontoActivity.this, PaymentActivity.class));
            }
        });
        if (getIntent().getExtras() != null) {
            band = getIntent().getExtras().getBoolean("showData");
            if (band) {
                showDatesPayment();
            }
        }

    }


    public void showDatesPayment() {
        mount = prefs.getString("com.nuestraapp.app.mount", "");
        paymentName = prefs.getString("com.nuestraapp.app.paymentName", "");
        bankName = prefs.getString("com.nuestraapp.app.bankName", "");
        mensaje = prefs.getString("com.nuestraapp.app.mensaje", "");
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.dialog_final, null);
        dialogo = new AlertDialog.Builder(this);
        dialogo.setView(promptsView);


        //dialogo.setCancelable(false);


        alertDialogo = dialogo.create();
        mountTxt = (TextView) promptsView.findViewById(R.id.montoTxt);
        paymentNameTxt = (TextView) promptsView.findViewById(R.id.payTxt);
        bankNameTxt = (TextView) promptsView.findViewById(R.id.bankTxt);
        mensajeTxt = (TextView) promptsView.findViewById(R.id.payerTxt);
        mountTxt.setText(mount);
        paymentNameTxt.setText(paymentName);
        bankNameTxt.setText(bankName);
        mensajeTxt.setText(mensaje);
       // alertDialogo.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        alertDialogo.show();


    }

    @Override
    public void onBackPressed() {
    }
}
