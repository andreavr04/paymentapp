package com.example.pay.paymentapp.DataModel;

import java.util.ArrayList;

/**
 * Created by Andrea on 30/07/2018.
 */

public class PayerResult {
    String payment_method_id;
    ArrayList<Mensaje> payer_costs;

    public String getPayment_method_id() {
        return payment_method_id;
    }

    public void setPayment_method_id(String payment_method_id) {
        this.payment_method_id = payment_method_id;
    }

    public ArrayList<Mensaje> getPayer_cost() {
        return payer_costs;
    }

    public void setPayer_cost(ArrayList<Mensaje> payer_cost) {
        this.payer_costs = payer_cost;
    }
}
