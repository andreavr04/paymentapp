package com.example.pay.paymentapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pay.paymentapp.Activitys.BankActivity;
import com.example.pay.paymentapp.Activitys.PaymentActivity;
import com.example.pay.paymentapp.DataModel.Payment;

import com.example.pay.paymentapp.R;
import com.example.pay.paymentapp.Servicios.PicassoTrustAll;
import com.squareup.picasso.Transformation;

import java.util.List;

/**
 * Created by Andrea on 25/07/2018.
 */

public class RecyclerItemAdapter extends RecyclerView.Adapter<RecyclerItemHolders> {


    private List<Payment> itemList;
    private Context context;
    private SharedPreferences prefs;
    private String autorization;
    private String ids = "";
    public static final String MESSAGE_PROGRESS = "message_progress";
    private static final int PERMISSION_REQUEST_CODE = 1;
    Activity activity;


    public RecyclerItemAdapter(Context context, List<Payment> itemList, Activity activity) {
        this.itemList = itemList;
        this.context = context;
        this.activity = activity;
        prefs = context.getSharedPreferences("com.nuestraapp.app", Context.MODE_PRIVATE);


    }

    @Override
    public RecyclerItemHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, null);
        RecyclerItemHolders rcv = new RecyclerItemHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerItemHolders holder, final int position) {
        if (itemList.get(position).getName() != null) {
            holder.title.setText(itemList.get(position).getName() + "");
        } else {
            holder.title.setText("Sin información");
        }
        /*if(itemList.get(position).getDescription()!=null) {
            holder.descripcion.setText(itemList.get(position).getDescription());
        }else{
            holder.descripcion.setText("Sin información");
        }*/
        Transformation transformation = new Transformation() {

            @Override
            public Bitmap transform(Bitmap source) {
                int targetWidth = 300;

                double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                int targetHeight = (int) (targetWidth * aspectRatio);
                Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                if (result != source) {
                    // Same bitmap is returned if sizes are the same
                    source.recycle();
                }
                return result;
            }

            @Override
            public String key() {
                return "transformation" + " desiredWidth";
            }
        };
        if (itemList.get(position).getSecure_thumbnail() != null) {
            if (!itemList.get(position).getSecure_thumbnail().equals("")) {

                String url = itemList.get(position).getThumbnail();
                PicassoTrustAll.getInstance(context)
                        .load(url)
                        .error(R.drawable.ic_dummy_ime_grande)
                        .transform(transformation)
                        .fit()
                        .into(holder.image, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                Log.e("Cargar imagen", "Success: ");
                                holder.progress.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                Log.e("Cargar imagen", "Error: ");
                                holder.progress.setVisibility(View.GONE);
                            }
                        });
            } else {
                holder.image.setImageResource(R.drawable.ic_dummy_ime_grande);
            }


        } else {
            holder.image.setImageResource(R.drawable.ic_dummy_ime_grande);
        }


        holder.viewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, BankActivity.class);
                prefs.edit().putString("com.nuestraapp.app.idpay",itemList.get(position).getId()).commit();
                prefs.edit().putString("com.nuestraapp.app.paymentName",itemList.get(position).getName()).commit();
                i.putExtra("idPay",itemList.get(position).getId());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

}
