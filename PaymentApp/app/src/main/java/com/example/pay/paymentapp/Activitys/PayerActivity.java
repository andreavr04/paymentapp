package com.example.pay.paymentapp.Activitys;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.pay.paymentapp.Adapters.PayerRecyclerAdapter;
import com.example.pay.paymentapp.Adapters.RecyclerItemAdapter;
import com.example.pay.paymentapp.DataModel.Mensaje;
import com.example.pay.paymentapp.DataModel.Payer;
import com.example.pay.paymentapp.DataModel.PayerResult;
import com.example.pay.paymentapp.DataModel.Payment;
import com.example.pay.paymentapp.R;
import com.example.pay.paymentapp.Servicios.ClienteRetrofit;
import com.example.pay.paymentapp.Servicios.PayApiServices;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PayerActivity extends AppCompatActivity {
    PayerRecyclerAdapter rcAdapter;
    String amount;
    String idBank;
    String idPay;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payer);
        prefs = getSharedPreferences("com.nuestraapp.app", Context.MODE_PRIVATE);
        amount=prefs.getString("com.nuestraapp.app.mount","");
        idBank=prefs.getString("com.nuestraapp.app.id","");
        idPay=prefs.getString("com.nuestraapp.app.idpay","");
        getPayers();
    }

    public void getPayers() {

        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(getString(R.string.BASE_URL));

        OkHttpClient client = ClienteRetrofit.getSSLConfig(this);
        Retrofit retrofit = builder.client(client).addConverterFactory(GsonConverterFactory.create()).build();
        retrofit.create(PayApiServices.class);
        getPayersApi(retrofit);


    }

    public void getPayersApi(Retrofit retrofit) {


        PayApiServices servicio = retrofit.create(PayApiServices.class);
        Call<ArrayList<PayerResult>> loginCall = servicio.getPayers("444a9ef5-8a6b-429f-abdf-587639155d88",amount,idPay,idBank);
        //Ejecuta una llamada asicnrona
        loginCall.enqueue(new Callback<ArrayList<PayerResult>>() {
            @Override
            public void onResponse(Call<ArrayList<PayerResult>> call, Response<ArrayList<PayerResult>> response) {

                if (response.isSuccessful()) {
                    gridImg(response.body().get(0).getPayer_cost());
                } else {

                }
            }

            @Override
            public void onFailure(Call<ArrayList<PayerResult>> call, Throwable t) {
                System.out.println("Falla al obtener respuesta: " + t.getMessage());

            }
        });


    }

    public void gridImg(ArrayList<Mensaje> lotesRes) {
        GridLayoutManager lLayout = new GridLayoutManager(PayerActivity.this, 1);
        RecyclerView rView = (RecyclerView) findViewById(R.id.recycler_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);
        rcAdapter = new PayerRecyclerAdapter(PayerActivity.this, lotesRes, PayerActivity.this);
        rView.setAdapter(rcAdapter);
    }

}
